import config

trainer = config.trainer
server_groups = config.server_groups
servers_ports = config.servers_ports
final_servers = config.final_servers

#getting groups by IP: top 2 ports
def calc_server_group():
    for i, row in trainer.iterrows():
        if row['IPV4_DST_ADDR'] == 'IPV4_DST_ADDR':
            continue
        # IP and port
        elif row['IPV4_DST_ADDR'] in server_groups and any(row['L4_DST_PORT'] in p for p in server_groups.get(row['IPV4_DST_ADDR'])):
            exisitingPortList = server_groups[row['IPV4_DST_ADDR']]
            for dict in exisitingPortList:
                for k,v in dict.items():
                    if(k == (row['L4_DST_PORT'])):
                        dict[row['L4_DST_PORT']] = dict.get(row['L4_DST_PORT']) + 1
        # IP and not port
        elif row['IPV4_DST_ADDR'] in server_groups and row['L4_DST_PORT'] not in server_groups:
            exisitingPortList = server_groups[row['IPV4_DST_ADDR']]
            newPort = {row['L4_DST_PORT']: 1}
            exisitingPortList.append(newPort)
            server_groups[row['IPV4_DST_ADDR']] = exisitingPortList
        # No IP and No port
        else:
            portList =[{row['L4_DST_PORT']:1}]
            server_groups[row['IPV4_DST_ADDR']] = portList

    for k, v in server_groups.items():
        counts = []
        for dict in v:
            port = list(dict.keys())[0]
            count = dict.get(port)
            counts.append(count)
        counts.sort(reverse=True)

        maxport = -1
        nextport = -1

        if(len(counts) == 1):
            for dict in v:
                port = list(dict.keys())[0]
                maxport = port
        elif(len(counts) >= 2):
            for dict in v:
                port = list(dict.keys())[0]
                count = dict.get(port)
                if((counts[0] == counts[1]) and (count == counts[0]) and (nextport == -1)):
                    nextport = port
                if(count == counts[0]):
                    maxport = port
                elif(count == counts[1]):
                    nextport = port

        servers_ports[k] = (maxport, nextport)
    return servers_ports

#getting flow percentage to get final servers.
# flow total = how many times we see a certain dest ip
# max flow total = how many times we see one of the two max ports with that IP
def refine(servers_ports: dict):
    for k,v in servers_ports.items():
        flow_total = 0
        max_port_total = 0
        for i, row in trainer.iterrows():
            # Check if this row is equal to our current IP
            if k == row['IPV4_DST_ADDR']:
                flow_total += 1
                # Check if this port is equal to one of the two max ports
                if row['L4_DST_PORT'] in v:
                    max_port_total += 1
        percentage = max_port_total / flow_total
        if percentage >= 0.9:
            final_servers.append(k)
    return final_servers

