import tensorflow as tf
import pandas as pd
from tensorflow.keras import mixed_precision
import config
import servergroups
import features
from matplotlib import pyplot as plt
mixed_precision.set_global_policy('mixed_float16')


server_groups = config.server_groups
servers_ports = config.servers_ports
final_servers = config.final_servers

#Read in CSV
trainer = config.trainer

def make_csv():
    for p in final_servers:
        print(p)
        config.ip.append(p)

        for k,v in features.reg_flow_size(final_servers).items():
            if k == p:
                config.reg_size_mean.append(v[0])
                config.reg_size_stdev.append(v[1])

        for k,v in features.uniq_flow_size(final_servers).items():
            if k == p:
                config.uniq_size.append(v)


dftrain = pd.read_csv('data.csv')
dfeval = pd.read_csv('flow.csv')
y_train = dftrain.pop('MALICIOUS')
y_eval = dfeval.pop('MALICIOUS')

def one_hot_cat_column(feature_name, vocab):
  return tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(feature_name,vocab))

NUM_EXAMPLES = len(y_train)

def make_input_fn(X, y, n_epochs=None, shuffle=True):
  def input_fn():
    dataset = tf.data.Dataset.from_tensor_slices((dict(X), y))
    if shuffle:
      dataset = dataset.shuffle(NUM_EXAMPLES)
    # For training, cycle thru dataset as many times as need (n_epochs=None).
    dataset = dataset.repeat(n_epochs)
    # In memory training doesn't use batching.
    dataset = dataset.batch(NUM_EXAMPLES)
    return dataset
  return input_fn


def main():
    server_ports = servergroups.calc_server_group()
    final_servers = servergroups.refine(server_ports)
    features.reg_flow_size(final_servers)
    features.uniq_flow_size(final_servers)
    features.detect_zombies(final_servers)
    make_csv()
    df = pd.DataFrame(config.csv)
    #saving the dataframe
    df.to_csv('flow.csv')

    NUMERIC_COLUMNS = ['REG_FLOW_SIZE_MEAN','REG_FLOW_SIZE_STDEV', 'UNIQ_FLOW_SIZE']

    feature_columns = []

    for feature_name in NUMERIC_COLUMNS:
        feature_columns.append(tf.feature_column.numeric_column(feature_name,
                                                                dtype=tf.float32))
    print(dftrain.shape[0])
    print(feature_columns)
    train_input_fn = make_input_fn(dftrain, y_train)
    eval_input_fn = make_input_fn(dfeval, y_eval, shuffle=False, n_epochs=1)

    n_batches = 1
    est = tf.estimator.BoostedTreesClassifier(feature_columns,
                                              n_batches_per_layer=n_batches)

    # The model will stop training once the specified number of trees is built, not
    # based on the number of steps.
    est.train(train_input_fn, max_steps=100)

    # Eval.
    result = est.evaluate(eval_input_fn)
    print(pd.Series(result))

    pred_dicts = list(est.predict(eval_input_fn))
    probs = pd.Series([pred['probabilities'][1] for pred in pred_dicts])
    print(probs)
    probs.plot(kind='bar', title='predicted probabilities')
    plt.show()


if __name__ == "__main__":
    main()

