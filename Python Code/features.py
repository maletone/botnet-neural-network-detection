import math
import config

trainer = config.trainer
server_groups = config.server_groups
servers_ports = config.servers_ports
final_servers = config.final_servers

def calcMeanAndSD(listBytes):
    total = len(listBytes)
    mean = sum(listBytes) / total
    deviations = [(x - mean) ** 2 for x in listBytes]
    variance = sum(deviations) / total
    std_dev = math.sqrt(variance)

    return (mean, std_dev)

#Get mean size of flows over time
#Statistical features
def reg_flow_size(final_servers):
    reg_flow_size_dict = {}
    for value in final_servers:
        listBytes = []
        for i, row in trainer.iterrows():
            if value == row['IPV4_DST_ADDR']:
                listBytes.append(int(row['IN_BYTES']))
                listBytes.append(int(row['OUT_BYTES']))
        mean_sd_tuple = calcMeanAndSD(listBytes)
        reg_flow_size_dict[value] = mean_sd_tuple
    return reg_flow_size_dict

#Get all uniq sizes of flows, and make a dict with size:count over time
#Unique flow sizes
def uniq_flow_size(final_servers):
    for i, row in trainer.iterrows():
        if row['IPV4_DST_ADDR'] == 'IPV4_DST_ADDR':
            continue
        # IP and flow size
        elif row['IPV4_DST_ADDR'] in server_groups and any(row['IN_BYTES'] in p for p in server_groups.get(row['IPV4_DST_ADDR'])):
            exisitingByteList = server_groups[row['IPV4_DST_ADDR']]
            for dict in exisitingByteList:
                for k,v in dict.items():
                    if(k == (row['IN_BYTES'])):
                        dict[row['IN_BYTES']] = dict.get(row['IN_BYTES']) + 1
        # IP and not flow size
        elif row['IPV4_DST_ADDR'] in server_groups and row['IN_BYTES'] not in server_groups:
            exisitingByteList = server_groups[row['IPV4_DST_ADDR']]
            newByte = {row['IN_BYTES']: 1}
            exisitingByteList.append(newByte)
            server_groups[row['IPV4_DST_ADDR']] = exisitingByteList
        # No IP and No flow size
        else:
            byteList =[{row['IN_BYTES']:1}]
            server_groups[row['IPV4_DST_ADDR']] = byteList

    for k, v in server_groups.items():
        counts = []
        # Get all the counts
        for dict in v:
            byte = list(dict.keys())[0]
            count = dict.get(byte)
            counts.append(count)
        counts.sort(reverse=True)
        maxCount = counts[0]

        # Find the entry that corresponds to the max
        for dict in v:
            byte = list(dict.keys())[0]
            count = dict.get(byte)
            if(count == maxCount):
                maxFlowDensity = count / sum(counts)

        servers_ports[k] = maxFlowDensity
    return servers_ports

#Check if a flow is sending one way and not getting any response back.
#Unmatched flow density
def detect_zombies(final_servers):
    source_dest_tuple_list = []
    for i, row in trainer.iterrows():
        if row['IPV4_DST_ADDR'] == 'IPV4_DST_ADDR':
            continue
        src_dest_tuple = (row['IPV4_SRC_ADDR'], row['IPV4_DST_ADDR'])
        source_dest_tuple_list.append(src_dest_tuple)

    final_list = source_dest_tuple_list

    for tup in final_list:
        src,des = tup
        reverseTuple = (des, src)
        if(reverseTuple in final_list):
            final_list.remove(tup)
            final_list.remove(reverseTuple)

    print(len(final_list))

    zombieIP = []

    for tup in final_list:
        src, des = tup
        if des in final_servers and des not in zombieIP:
            zombieIP.append(des)

    return zombieIP

ip = config.ip
reg_size_mean = config.reg_size_mean
reg_size_stdev = config.reg_size_stdev
uniq_size = config.uniq_size
zombie = config.zombie
csv = config.csv