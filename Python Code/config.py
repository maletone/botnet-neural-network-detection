import pandas as pd

server_groups = {}
servers_ports = {}
final_servers = []

ip = []
reg_size_mean = []
reg_size_stdev = []
uniq_size = []
zombie = []
csv = {'IP': ip, 'REG_FLOW_SIZE_MEAN': reg_size_mean, 'REG_FLOW_SIZE_STDEV': reg_size_stdev, 'UNIQ_FLOW_SIZE': uniq_size}

#Read in CSV
trainer = pd.read_csv('13.flows', sep="|", names=["IPV4_SRC_ADDR","IPV4_DST_ADDR", "L4_DST_PORT", "IN_BYTES", "OUT_BYTES", "FLOW_DURATION_MILLISECONDS"])

